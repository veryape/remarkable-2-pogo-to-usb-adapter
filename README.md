# remarkable-2-pogo-to-usb-adapter

OpenSCAD design, STL, and KiCad project for a slide-on pogo-to-USB-C adapter for the reMarkable 2.

This adapter is designed for use in OTG mode. It should not be considered
equivalent to a microUSB breakout adapter, which can be used to access the
device in "mass storage" mode in order to perform firmware replacement and
recovery. Its primary purpose is to provide a means to connect USB-C devices to
the reMarkable 2, nothing else.

To put the pogo pins into OTG mode so that USB devices can be connected to the
reMarkable, the following must first be run on the command line, either via ssh
or via a terminal emulator such as yaft:

```
echo 2 > /sys/otgcontrol/control/otg1_controllermode
```

This value does not persist between reboots.

Not all USB devices are compatible with the reMarkable 2's stock kernel. E.g.,
I have not yet found a way to get a thumbdrive or microSD adapter to work.

Also, be aware that although the physical connection seems to be secure enough,
the tablet does still lose connection sometimes. Removing the adapter,
re-running the shell command above, and re-attaching the adapter, seems to fix
things when this happens.

## Components

- 3D printed case (see `case/`)
- PCB (see `pcb/`)
- [Adafruit USB-C breakout board](https://www.adafruit.com/product/4090)
- [5-pin 1-row Mill-Max right-angle through-hole pogo pins](https://www.mouser.com/ProductDetail/Mill-Max/829-22-005-20-002101?qs=yatApJqb7mRbGFGuLIAXxw%3D%3D)
- 4 M2x8mm countersunk screws
- 2 4x2mm magnets (optional)

## Photos

![action shot](images/action.jpg)

Using the adapter with a keyboard and a tablet stand to type in vim.

![back shot](images/back.jpg)

What it looks like from the back with a USB-C OTG adapter and a keyboard
plugged in.

![front shot](images/front.jpg)

Holding it and looking at it from the front.

![prototypes](images/prototypes.jpg)

All the little 3D prints (and 1 PCB prototype) it took to develop this design.
