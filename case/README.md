# case

The OpenSCAD file is provided to make it easy to create variations on this
design, though there are more un-named values in it than I'd like.

The STLs should be printed as oriented, with supports on. To fit on your build
plate, it might be necessary to rotate the pieces 45 degrees on the Z-axis.

Two 4x2mm magnets can be embedded in the case to provide additional strength to
the coupling, but these should be considered optional. The friction-fit is
sufficient to keep a reliable connection.

If you choose to include the magnets, embed them during printing by adding a
pause in the print's gcode at the last layer before the magnet holes get closed
off. Make sure to get the polarity right! Check against your reMarkable 2 to
ensure they don't end up *repelling* the tablet's spine.
