// Minkowski is way faster than hull
module roundedCube(xyz, r=5, center=false, use_cylinder=false, $fn=20) {
  module _corner() {
      if (use_cylinder) cylinder(r=r, h=r, $fn=$fn);
      else sphere(r=r, $fn=$fn);
  }
  translate([r, r, use_cylinder ? 0 : r])
  minkowski() {
    cube([xyz.x - r*2, xyz.y - r*2, max(0.1, xyz.z - (use_cylinder ? r : r*2))]);
    _corner();
  }
}
